import * as cdk from 'aws-cdk-lib';
import { Template, Match } from 'aws-cdk-lib/assertions';
import * as InsuranceBankingApi from '../lib/insurance_banking_api-stack';
import { IProps } from '../models/IPropsBase';
import { AppEnvironment } from '../properties/AppEnvironment';

const environment = "staging";

const props: IProps = {
    applicationName: "insurance-banking-api",
    environment: environment
}

test('SQS Queue and SNS Topic Created', () => {
  const app = new cdk.App();
  // WHEN
  //const stack = new InsuranceBankingApi.InsuranceBankingApiStack(app, 'MyTestStack', AppEnvironment.getEnv(props)!);
  // THEN
  //const template = Template.fromStack(stack);

});
