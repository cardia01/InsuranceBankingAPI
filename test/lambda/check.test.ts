import app from "../../lambda/appdev";
import request from "supertest";

afterAll(async () => {
	await new Promise<void>(resolve => setTimeout(() => resolve(), 500));
});

describe('InsuranceBankingAPI - /Cheques module', () => {
  
  test('400 - Cheques endpoint', async () => {
    const body = {
        test: "test" ,
    }
    const res = await request(app).post("/Cheques").send(body);
    const resJson = JSON.parse(res.text);

    expect(res.statusCode).toEqual(400);
    expect(resJson.details[0].message).toEqual('"publicID" is required');
  });

});

