#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib';
import { IProps } from '../models/IPropsBase';
import { InsuranceBankingPipelineStack } from '../pipeline/pipeline-stack';
import { AppEnvironment } from '../properties/AppEnvironment';
import { ParameterNames } from '../properties/ParameterNames';

/**
 * To bootstrap the application
 * cdk bootstrap -c env=staging
 * cdk bootstrap -c env=prod
 * 
 * To validate changes locallly
 * cdk synth -c env=staging InsuranceBankingApiStack 
 * cdk synth -c env=prod InsuranceBankingApiStack 
 * 
 * To deploy 
 * cdk deploy -c env=staging InsuranceBankingApiStack 
 * cdk deploy -c env=prod InsuranceBankingApiStack
 */

const app = new cdk.App();
const environment = app.node.tryGetContext("env");

const props: IProps = {
    applicationName: "insurance-banking-api",
    environment: environment
}

new InsuranceBankingPipelineStack(app, ParameterNames.STACK_NAME, AppEnvironment.getEnv(props)!);
