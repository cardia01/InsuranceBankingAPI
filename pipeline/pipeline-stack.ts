import * as cdk from 'aws-cdk-lib';
import { CodePipeline, CodePipelineSource, ShellStep } from "aws-cdk-lib/pipelines";
import { Construct } from 'constructs';
import { IPropsApp } from '../models/IPropsApp';
import { PipelineStage } from './stages/pipeline-stages';

export class InsuranceBankingPipelineStack extends cdk.Stack {
    
    constructor(scope: Construct, id: string, props: IPropsApp) {
        super(scope, id, props);

        // Creates a code pipeline repository called 'amaabca/InsuranceBankingAPI'
        // Pipeline declaration. This sets the initial structure of our pipeline
        const pipeline = new CodePipeline(this, id, {
            pipelineName: props.pipeline.pipelineName,
            synth: new ShellStep('synth', {
                input: CodePipelineSource.gitHub(props.pipeline.repositoryName, props.pipeline.branchName),
                commands: ['npm install',
                    'npm ci',
                    'npm run build',
                    'npm run test',
                    `npx cdk synth -c env=${props.propsBase?.environment}`],
                primaryOutputDirectory: props.pipeline.primaryOutputDirectory
            })
        });
        pipeline.addStage(new PipelineStage(this, `${id}-${props.propsBase?.environment}`, props));
    }
}