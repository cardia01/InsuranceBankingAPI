import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { InsuranceBankingApiStack } from '../../lib/insurance_banking_api-stack';
import { IPropsApp } from '../../models/IPropsApp';

export class PipelineStage extends cdk.Stage {
    constructor(scope: Construct, stageName: string, props: IPropsApp) {
        super(scope, stageName, props);

        // defines the all the stack base on app insurance_banking_api-stack.ts
        const stack = new InsuranceBankingApiStack(this, stageName, props);
    }
}