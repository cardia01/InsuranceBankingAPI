import { IPropsApp } from "../models/IPropsApp";
import { IProps } from "../models/IPropsBase";
import { ParameterNames } from "./ParameterNames";

export class AppEnvironment {

    public static staging: IPropsApp = {
        pipeline: {
            pipelineName: 'InsuranceBankingPipelineStack-'+ ParameterNames.STAGING_ENV,
            repositoryName: 'amaabca/InsuranceBankingAPI',
            branchName: 'develop',
            primaryOutputDirectory: 'cdk.out'
        },
        lambda: {
            lambdaName: 'InsuranceBankingLambdaAPI-'+ ParameterNames.STAGING_ENV,
            lambdaFolder: './lambda/app.ts',
            apiGatewayName: 'InsuranceBankingApigateway-'+ ParameterNames.STAGING_ENV,
            handlerName: 'handler'
        },
        vpc: {
            vpcId: "vpc-07cb5e40894f21fb9",
            subnetId1: "subnet-06ad102ef1baf7072",
            subnetId2: "subnet-0576c5f4faf8f12c5",
        },
        credentialdb: {
            dbUrl: '10.93.3.170',
            dbname: 'GW_Integration_uat_ama',
            user: 'gw_integr',
            pass: 'guidewire',
            port: 1433,
        },
        env: { account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION },
    }

    public static prod: IPropsApp = {
        pipeline: {
            pipelineName: 'InsuranceBankingPipelineStack-'+ ParameterNames.PROD_ENV,
            repositoryName: 'amaabca/InsuranceBankingAPI',
            branchName: 'master',
            primaryOutputDirectory: 'cdk.out'
        },
        lambda: {
            lambdaName: 'InsuranceBankingLambdaAPI-'+ ParameterNames.PROD_ENV,
            lambdaFolder: './lambda/app.ts',
            apiGatewayName: 'InsuranceBankingApigateway-'+ ParameterNames.PROD_ENV,
            handlerName: 'handler'
        },
        vpc: {
            vpcId: "vpc-091be0677b1708fb7",
            subnetId1: "subnet-0215ec309c9e9e73d",
            subnetId2: "subnet-01d1477bd0c716ecd",
        },
        credentialdb: {
            dbUrl: 'ins-t-gwdb-v01.CORP.ADS',
            dbname: 'GW_Integration_uat_ama',
            user: 'gw_integr',
            pass: 'guidewire',
            port: 1433,
        },
        env: { account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION },
    }

    private static setPropsValues(props: IProps, propsApp: IPropsApp) {
        propsApp.propsBase = props;
        return propsApp;
    }

    public static getEnv(props: IProps): IPropsApp | undefined {
        if (props.environment.toLowerCase() === ParameterNames.STAGING_ENV) return AppEnvironment.setPropsValues(props, AppEnvironment.staging);
        if (props.environment.toLowerCase() === ParameterNames.PROD_ENV) return AppEnvironment.setPropsValues(props, AppEnvironment.prod);
        return undefined;
    }

}