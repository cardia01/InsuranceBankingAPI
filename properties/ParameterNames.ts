export class ParameterNames {    
    public static readonly AWS_REGION = "ca-central-1";
    public static readonly STAGING_ENV = "staging";
    public static readonly AWS_STAGING_ACCOUNT = "005862276393";
    public static readonly AWS_PROD_ACCOUNT = "005862276393";
    public static readonly PROD_ENV = "prod";
    public static readonly STACK_NAME = "InsuranceBankingApiStack";
}