# Welcome to Insurance-BankingAPI

This API project is building to query and persist GW_Integration_ENV database models as HTTP endpoints with API Manager and AWS CDK framework (Lambda function - IAM - CodePipeline - CloudFormation).

This project contains source code and supporting files for a serverless application that you can deploy with CDK app with an instance of a stack (`InsuranceBankingApiStack`). It includes the following files and folders.

- **bin** - is the entrypoint of the CDK application. It will load the stack defined in `lib/insurance_banking_api-stack.ts`.
- **lambda** - is where your CDK application’s lambda function is defined. This folder mantain all the changes on serverless express lambda function.
- **lib** - is where your CDK application’s main stack is defined. This is the file we’ll be spending most of our time in.
- **models** - is where CDK application defined the `IPropsBase.ts` and `IPropsApp.ts` interfaces.
- **node_modules** - is maintained by npm and includes all your project’s dependencies.
- **pipeline** - is where CDK application defined the pipeline process. This folder clone the repo into AWS and deploy the app and all the services needed.
- **properties** - is where CDK application defined gobal variables.
- **test** - is where CDK application defined, run and validate the unit testing code.
- **util** - is where CDK application defined the lambda helper.
- **package.json** - is your npm module manifest. It includes information like the name of your app, version, dependencies and build scripts like “watch” and “build” (`package-lock.json` is maintained by npm).
- **cdk.json** - tells the toolkit how to run your app. In our case it will be `npx ts-node bin/insurance_banking_api.ts`.
- **tsconfig.json** - your project’s typescript configuration.
- **.gitignore and .npmignore** - tell git and npm which files to `include/exclude` from source control and when publishing this module to the package manager.

The application uses several AWS resources, including Lambda functions. These resources are defined in the `lib/insurance_banking_api-stack.ts` file in this project. You can update the template to add AWS resources through the same deployment process that updates your application code.

# Architecture 

![alt text](/doc/architecture/aws-architecture.jpg "AWS Architecture")

# Pre-requisites

* AWS Account -  [AWS Account](http://aws.ama.ab.ca/)
* AWS CLI - [AWS CLI](https://cdkworkshop.com/15-prerequisites/100-awscli.html)
* CDK - [Install CDK](https://cdkworkshop.com/15-prerequisites/500-toolkit.html)
* NodeJs - [NodeJs 14.x installed](https://nodejs.org/en/)
* TypeScript - [TypeScript](https://www.typescriptlang.org/)
* Docker - [Install Docker community edition](https://hub.docker.com/search/?type=edition&offering=community)
* MSsql - [Install microsoft sql server](https://www.microsoft.com/en-ca/sql-server/sql-server-downloads)

## Frameworks TypeScript

* aws-sdk -  [npm aws-sdk](https://www.npmjs.com/package/aws-sdk) - You can also build a custom browser SDK with your specified set of AWS services.
* aws-cdk-lib -  [npm aws-cdk-lib](https://www.npmjs.com/package/aws-cdk-lib) - The AWS CDK construct library provides APIs to define your CDK application and add CDK constructs to the application.
* Constructs -  [npm constructs](https://www.npmjs.com/package/constructs) - Constructs can be composed together to form higher-level building blocks which represent more complex state.
* Express -  [npm express](https://www.npmjs.com/package/express) - Fast, unopinionated, minimalist web framework for node.
* Serverless-http -  [npm serverless-http](https://www.npmjs.com/package/serverless-http) - This module allows you to 'wrap' your API for serverless use
* Joi -  [npm joi](https://www.npmjs.com/package/joi) - The most powerful schema description language and data validator for JavaScript.
* Sequelize - [sequelize](https://sequelize.org/) - Sequelize is a modern TypeScript and Node.js ORM for SQL Server, and more. Featuring solid transaction support, relations, eager and lazy loading.

## Deployment Staging

To build and deploy CDK application for the first time to the aws cloud, run the following in your shell:

```bash
  cd InsuranceBankingAPI
  cdk bootstrap -c env=staging 
```

```bash
  cdk synth -c env=staging 
```

```bash
  cdk deploy -c env=staging 
```

## Deployment Prod

To build and deploy CDK application for the first time to the aws cloud, run the following in your shell:

```bash
  cd InsuranceBankingAPI
  cdk bootstrap -c env=prod 
```

```bash
  cdk synth -c env=prod 
```

```bash
  cdk deploy -c env=prod 
```

# Useful CDK commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template

# Getting Started Lambda Function

Clone this repository

```
git clone https://github.com/amaabca/InsuranceBankingAPI.git
```

## Run the application locally

Install dependencies.

```bash
$ cd InsuranceBankingAPI
InsuranceBankingAPI$ npm install
```

Build and run.

```bash
InsuranceBankingAPI$ npm run servedev
```

Output:
```bash
> insurance_banking_api@0.1.0 servedev
> nodemon lambda/appdev.ts

[nodemon] 2.0.16
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: ts,json
[nodemon] starting `ts-node lambda/appdev.ts`
Server running on : http://localhost:3000
```

## Lambda Structure Layer

```
lambda/
└───app.ts            # start serverles app
└───appdev.ts         # start dev express app
└───index.ts          # Application entry point
└───routes            # Application routes / endpoints
└───controllers       # Express route controllers for all the endpoints of the app
└───repositories      # All the database interaction logic is here
└───db                # DB related files like connection / seed data
└───handlers          # Common logic
└───models            # DB Models (Postgress)
└───validators        # API Request object validations

```

## Features

- POST operations for /Cheques
- POST operations for /Cheques/BulkPrint
- POST operations for /PADBC
- POST operations for /PADCC
- POST operations for /DataBanking
- POST operations for /BulkChequeDetail/GetNextChequeNumber
- POST operations for /BulkChequeDetail/SaveBulkChequeDetail
- REST API Request object validations - Basic

## REST Services

The application exposes a few REST endpoints

`HTTP` `POST` /Cheques

`HTTP` `POST` /Cheques/BulkPrint

`HTTP` `POST` /PADBC

`HTTP` `POST` /PADCC

`HTTP` `POST` /DataBanking

`HTTP` `POST` /BulkChequeDetail/GetNextChequeNumber

`HTTP` `POST` /BulkChequeDetail/SaveBulkChequeDetail

# Unit tests

Tests are defined in the `tests` folder in this project.

# Recommended / Preferred

[VSCode](https://code.visualstudio.com/download)


