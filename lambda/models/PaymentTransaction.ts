import { sequelize } from './../db/db';
import { Model, DataTypes } from 'sequelize';

export class PaymentTransaction extends Model {
    public transaction_SQLKEY!:	number;
    public file_Date!:	Date;
    public batch_FI_Number!: string;
    public batch_FI_Name!: string;
    public sequence_Number!: string;
    public payment_Trace!: string;
    public filler_Reserved1!: string;
    public billing_Account_Number!: string;
    public payment_Amount!: number;
    public bank_Number!: string;
    public payment_Date!: Date;
    public filler_Reserved2!: string;
    public value_Date!: Date;
    public account_Holder!: string;
    public filler_Reserved3!: string;
    public ccIN_Number!: string;
    public file_Number!: string;
    public batch_Seq_Number!: string;
    public processStatus!: string;
    public processDate!: Date;
    public gwAccount_Number!: string;
}

PaymentTransaction.init(
    {
      transaction_SQLKEY: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
      file_Date: { type: DataTypes.DATE },
      batch_FI_Number: { type: DataTypes.STRING },
      batch_FI_Name: { type: DataTypes.STRING },
      sequence_Number: { type: DataTypes.STRING },
      payment_Trace: { type: DataTypes.STRING },
      filler_Reserved1: { type: DataTypes.STRING },
      billing_Account_Number: { type: DataTypes.STRING },
      payment_Amount: { type: DataTypes.DECIMAL },
      bank_Number: { type: DataTypes.STRING },
      payment_Date: { type: DataTypes.DATE },
      filler_Reserved2: { type: DataTypes.STRING },
      value_Date: { type: DataTypes.DATE },
      account_Holder: { type: DataTypes.STRING },
      filler_Reserved3: { type: DataTypes.STRING },
      ccIN_Number: { type: DataTypes.STRING },
      file_Number: { type: DataTypes.STRING },
      batch_Seq_Number: { type: DataTypes.STRING },
      processStatus: { type: DataTypes.STRING },
      processDate: { type: DataTypes.DATE },
      gwAccount_Number: { type: DataTypes.STRING },
    },
    {
      sequelize,
      tableName: 'Payment_Transaction',
    },
  );
  