import { sequelize } from '../db/db';
import { Model, DataTypes } from 'sequelize';

export class CCBCChecks extends Model {
  public id!: bigint;
  public publicID!: string;
  public sourceSystem!: string;
  public amount!: number;
  public checkType!: string;
  public checkDate!: string;
  public checkNumber!: string;
  public payee_Line_1!: string;
  public payee_Line_2!: string;
  public payee_Line_3!: string;
  public payee_Line_4!: string;
  public payee_Line_5!: string;
  public claimPolicyNum!: string;
  public chequeNote1!: string;
  public chequeNote2!: string;
  public createDate!: Date;
  public senderMsgID!: string;
  public processedDate!: Date;
  public uwCompany!: string;
  public reason!: string;
  public policyNo!: string;
  public claimNo!: string;
  public accountNo!: string;
  public refNo!: string;
  public category!: string;
  public exposureType!: string;
  public lossDate!: Date;
  public adjusterFirst!: string;
  public adjusterLast!: string;
  public adjusterBranchNo!: string;
  public paymentType!: string;
  public insuredName!: string;
  public cc_bc_dateStamp!: Date;
}

CCBCChecks.init(
  {
    id: { type: DataTypes.BIGINT, primaryKey: true, autoIncrement: true },
    publicID: { type: DataTypes.STRING },
    sourceSystem: { type: DataTypes.STRING },
    amount: { type: DataTypes.DECIMAL },
    checkType: { type: DataTypes.STRING },
    checkDate: { type: DataTypes.STRING },
    checkNumber: { type: DataTypes.STRING },
    payee_Line_1: { type: DataTypes.STRING },
    payee_Line_2: { type: DataTypes.STRING },
    payee_Line_3: { type: DataTypes.STRING },
    payee_Line_4: { type: DataTypes.STRING },
    payee_Line_5: { type: DataTypes.STRING },
    claimPolicyNum: { type: DataTypes.STRING },
    chequeNote1: { type: DataTypes.STRING },
    chequeNote2: { type: DataTypes.STRING },
    createDate: { type: DataTypes.DATE },
    senderMsgID: { type: DataTypes.STRING },
    processedDate: { type: DataTypes.DATE },
    uwCompany: { type: DataTypes.STRING },
    reason: { type: DataTypes.STRING },
    policyNo: { type: DataTypes.STRING },
    claimNo: { type: DataTypes.STRING },
    accountNo: { type: DataTypes.STRING },
    refNo: { type: DataTypes.STRING },
    category: { type: DataTypes.STRING },
    exposureType: { type: DataTypes.STRING },
    lossDate: { type: DataTypes.DATE },
    adjusterFirst: { type: DataTypes.STRING },
    adjusterLast: { type: DataTypes.STRING },
    adjusterBranchNo: { type: DataTypes.STRING },
    paymentType: { type: DataTypes.STRING },
    insuredName: { type: DataTypes.STRING },
    cc_bc_dateStamp: { type: DataTypes.DATE },
  },
  {
    sequelize,
    tableName: 'CC_BC_Checks',
  },
);
