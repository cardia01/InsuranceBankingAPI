
export class Check {
  public id!: bigint;
  public publicID!: string;
  public sourceSystem!: string;
  public amount!: number;
  public checkType!: string;
  public checkDate!: string;
  public checkNumber!: string;
  public payee_Line_1!: string;
  public payee_Line_2!: string;
  public payee_Line_3!: string;
  public payee_Line_4!: string;
  public payee_Line_5!: string;
  public claimPolicyNum!: string;
  public chequeNote1!: string;
  public chequeNote2!: string;
  public createDate!: Date;
  public senderMsgID!: string;
  public processedDate!: Date;
  public uwCompany!: string;
  public reason!: string;
  public policyNo!: string;
  public claimNo!: string;
  public accountNo!: string;
  public refNo!: string;
  public category!: string;
  public exposureType!: string;
  public lossDate!: Date;
  public adjusterFirst!: string;
  public adjusterLast!: string;
  public adjusterBranchNo!: string;
  public paymentType!: string;
  public insuredName!: string;
  public cc_bc_dateStamp!: Date;
}
