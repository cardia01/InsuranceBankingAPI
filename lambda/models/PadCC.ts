import { sequelize } from './../db/db';
import { Model, DataTypes } from 'sequelize';

export class PadCC extends Model {
  public id!: bigint;
  public amount!: number;
  public claimantName!: string;
  public claimNumber!: string;
  public paymentID!: string;
  public bankNumber!: string;
  public bankBranchNumber!: string;
  public bankAccountNumber!: string;
  public bankAccountHolderName!: string;
  public requestDate!: Date;
  public dueDate!: Date;
  public processedDate!: Date;
  public recordType!: string;
  public cc_dateStamp!: Date;
}

PadCC.init(
  {
    id: { type: DataTypes.BIGINT, primaryKey: true, autoIncrement: true },
    amount: { type: DataTypes.DECIMAL },
    claimantName: { type: DataTypes.STRING },
    claimNumber: { type: DataTypes.STRING },
    paymentID: { type: DataTypes.STRING },
    bankNumber: { type: DataTypes.STRING },
    bankBranchNumber: { type: DataTypes.STRING },
    bankAccountNumber: { type: DataTypes.STRING },
    bankAccountHolderName: { type: DataTypes.STRING },
    requestDate: { type: DataTypes.DATE },
    dueDate: { type: DataTypes.DATE },
    processedDate: { type: DataTypes.DATE },
    recordType: { type: DataTypes.STRING },
    cc_dateStamp: { type: DataTypes.DATE },
  },
  {
    sequelize,
    tableName: 'CC_PAD',
  },
);
