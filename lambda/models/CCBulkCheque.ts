import { sequelize } from '../db/db';
import { Model, DataTypes } from 'sequelize';

export class CCBulkCheque extends Model {
  public id!: bigint;
  public publicID!: string;
  public checkDate!: string;
  public checkNumber!: string;
  public amount!: number;
  public payee_Line_1!: string;
  public payee_Line_2!: string;
  public payee_Line_3!: string;
  public payee_Line_4!: string;
  public payee_Line_5!: string;
  public chequeNote1!: string;
  public chequeNote2!: string;
  public createDate!: Date;
  public senderMsgID!: string;
  public processedDate!: Date;
  public uwCompany!: string;
  public reason!: string;
  public policyNo!: string;
  public claimNo!: string;
  public accountNo!: string;
  public refNo!: string;
  public category!: string;
  public exposureType!: string;
  public lossDate!: Date;
  public adjusterFirst!: string;
  public adjusterLast!: string;
  public adjusterBranchNo!: string;
  public insuredName!: string;
  public vendorPublicID!: string;
}

CCBulkCheque.init(
  {
    id: { type: DataTypes.BIGINT, primaryKey: true, autoIncrement: true },
    publicID: { type: DataTypes.STRING },
    checkDate: { type: DataTypes.STRING },
    checkNumber: { type: DataTypes.STRING },
    amount: { type: DataTypes.DECIMAL },
    payee_Line_1: { type: DataTypes.STRING },
    payee_Line_2: { type: DataTypes.STRING },
    payee_Line_3: { type: DataTypes.STRING },
    payee_Line_4: { type: DataTypes.STRING },
    payee_Line_5: { type: DataTypes.STRING },
    chequeNote1: { type: DataTypes.STRING },
    chequeNote2: { type: DataTypes.STRING },
    createDate: { type: DataTypes.DATE },
    senderMsgID: { type: DataTypes.STRING },
    processedDate: { type: DataTypes.DATE },
    uwCompany: { type: DataTypes.STRING },
    reason: { type: DataTypes.STRING },
    policyNo: { type: DataTypes.STRING },
    claimNo: { type: DataTypes.STRING },
    accountNo: { type: DataTypes.STRING },
    refNo: { type: DataTypes.STRING },
    category: { type: DataTypes.STRING },
    exposureType: { type: DataTypes.STRING },
    lossDate: { type: DataTypes.DATE },
    adjusterFirst: { type: DataTypes.STRING },
    adjusterLast: { type: DataTypes.STRING },
    adjusterBranchNo: { type: DataTypes.STRING },
    insuredName: { type: DataTypes.STRING },
    vendorPublicID: { type: DataTypes.STRING },
  },
  {
    sequelize,
    tableName: 'CC_Bulk_Cheque',
  },
);
