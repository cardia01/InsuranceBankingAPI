import { sequelize } from './../db/db';
import { Model, DataTypes } from 'sequelize';

export class BulkChequeDetail extends Model {
  public id!: bigint;
  public publicID!: string;
  public checkNumber!: string;
  public claimNo!: string;
  public refNo!: string;
  public createDate!: Date;
  public amount!: number;
  public processedDate!: Date;
  public payee_Line_1!: string;
  public payee_Line_2!: string;
  public payee_Line_3!: string;
  public payee_Line_4!: string;
  public payee_Line_5!: string;
  public processStatus!: string;
  public vendorPublicID!: string;
  public dateStamp!: Date;
  public cc_dateStamp!: Date;
}

BulkChequeDetail.init(
  {
    id: { type: DataTypes.BIGINT, primaryKey: true, autoIncrement: true },
    publicID: { type: DataTypes.STRING },
    checkNumber: { type: DataTypes.STRING },
    claimNo: { type: DataTypes.STRING },
    refNo: { type: DataTypes.STRING },
    createDate: { type: DataTypes.DATE },
    amount: { type: DataTypes.DECIMAL },
    processedDate: { type: DataTypes.DATE },
    payee_Line_1: { type: DataTypes.STRING },
    payee_Line_2: { type: DataTypes.STRING },
    payee_Line_3: { type: DataTypes.STRING },
    payee_Line_4: { type: DataTypes.STRING },
    payee_Line_5: { type: DataTypes.STRING },
    processStatus: { type: DataTypes.STRING },
    vendorPublicID: { type: DataTypes.STRING },
    dateStamp: { type: DataTypes.DATE },
    cc_dateStamp: { type: DataTypes.DATE },
  },
  {
    sequelize,
    tableName: 'Bulk_Cheque_Detail',
  },
);
