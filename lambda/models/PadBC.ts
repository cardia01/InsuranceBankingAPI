import { sequelize } from './../db/db';
import { Model, DataTypes } from 'sequelize';

export class PadBC extends Model {
  public id!: bigint;
  public amount!: number;
  public insuredName!: string;
  public termNumber!: number;
  public policyEffDate!: Date;
  public policyNumber!: string;
  public publicID!: string;
  public bankNumber!: string;
  public bankBranchNumber!: string;
  public bankAccountNumber!: string;
  public bankAccountHolderName!: string;
  public requestDate!: Date;
  public dueDate!: Date;
  public processedDate!: Date;
  public recordType!: string;
  public fGWProcessDate!: Date;
  public uwCompany!: string;
  public bc_dateStamp!: Date;
}

PadBC.init(
  {
    id: { type: DataTypes.BIGINT, primaryKey: true, autoIncrement: true },
    amount: { type: DataTypes.DECIMAL },
    insuredName: { type: DataTypes.STRING },
    termNumber: { type: DataTypes.INTEGER },
    policyEffDate: { type: DataTypes.DATE },
    policyNumber: { type: DataTypes.STRING },
    publicID: { type: DataTypes.STRING },
    bankNumber: { type: DataTypes.STRING },
    bankBranchNumber: { type: DataTypes.STRING },
    bankAccountNumber: { type: DataTypes.STRING },
    bankAccountHolderName: { type: DataTypes.STRING },
    requestDate: { type: DataTypes.DATE },
    dueDate: { type: DataTypes.DATE },
    processedDate: { type: DataTypes.DATE },
    recordType: { type: DataTypes.STRING },
    fGWProcessDate: { type: DataTypes.DATE },
    uwCompany: { type: DataTypes.STRING },
    bc_dateStamp: { type: DataTypes.DATE },
  },
  {
    sequelize,
    tableName: 'BC_PAD',
  },
);
