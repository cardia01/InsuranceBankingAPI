import { Router } from 'express';
import PadCCCtrl from '../controllers/PadCCCtrl';
import { PadCCValidator, padCCSchema } from '../validators/PadCCValidator';

class PadCCsRoutes {
  router = Router();
  PadCCCtrl = new PadCCCtrl();
  PadCCValidator = new PadCCValidator();

  constructor() {
    this.intializeRoutes();
  }
  intializeRoutes() {
    this.router.route('/')
      .post(
        this.PadCCValidator.validateBody(padCCSchema),
        this.PadCCCtrl.savePadCC
      );
    
  }
}

export default new PadCCsRoutes().router;