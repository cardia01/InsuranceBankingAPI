import { Application } from 'express';

import cheksRouter from './ChecksRoutes';
import padBCRouter from './PadBCRoutes';
import padCCRouter from './PadCCRoutes';
import dataBankingRouter from './DataBankingRouter';
import bulkChequeDetailRouter from './BulkChequeDetailRouter';

export default class Routes {

  constructor(app: Application) {

    app.use('/Cheques', cheksRouter);

    app.use('/PADBC', padBCRouter);
    
    app.use('/PADCC', padCCRouter);

    app.use('/DataBanking', dataBankingRouter);

    app.use('/BulkChequeDetail', bulkChequeDetailRouter);
    
  }
}
