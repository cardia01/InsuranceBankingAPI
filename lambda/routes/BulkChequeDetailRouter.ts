import { Router } from 'express';
import BulkChequeDetailCtrl from '../controllers/BulkChequeDetailCtrl';
import { BulkChequeDetailValidator, bulkChequeDetailSchema, getBulkChequeDetailSchema } from '../validators/BulkChequeDetailValidator';

class CheksRoutes {
  router = Router();
  BulkChequeDetailCtrl = new BulkChequeDetailCtrl();
  BulkChequeDetailValidator = new BulkChequeDetailValidator();

  constructor() {
    this.intializeRoutes();
  }
  intializeRoutes() {
    this.router.route('/SaveBulkChequeDetail')
      .post(
        this.BulkChequeDetailValidator.validateBody(bulkChequeDetailSchema),
        this.BulkChequeDetailCtrl.saveBulkChequeDetail
      );

      this.router.route('/GetNextChequeNumber')
      .post(
        this.BulkChequeDetailValidator.validateBody(getBulkChequeDetailSchema),
        this.BulkChequeDetailCtrl.getBulkChequeDetail
      );
  }
}

export default new CheksRoutes().router;