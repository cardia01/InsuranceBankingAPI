import { Router } from 'express';
import DataBankingCtrl from '../controllers/DataBankingCtrl';
import { DataBankingValidator, dataBankingSchema } from '../validators/DataBankingValidator';

class DataBankingsRoutes {
  router = Router();
  DataBankingCtrl = new DataBankingCtrl();
  DataBankingValidator = new DataBankingValidator();

  constructor() {
    this.intializeRoutes();
  }
  intializeRoutes() {
    this.router.route('/')
      .post(
        this.DataBankingValidator.validateBody(dataBankingSchema),
        this.DataBankingCtrl.getDataBanking
      );
    
  }
}

export default new DataBankingsRoutes().router;