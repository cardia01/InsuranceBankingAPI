import { Router } from 'express';
import ChecksCtrl from '../controllers/ChecksCtrl';
import { ChecksValidator, checksSchema } from '../validators/ChecksValidator';
import { ChecksBulkValidator, checksBulkSchema } from '../validators/ChecksBulkValidator';

class CheksRoutes {
  router = Router();
  ChecksCtrl = new ChecksCtrl();
  ChecksValidator = new ChecksValidator();
  ChecksBulkValidator = new ChecksBulkValidator();

  constructor() {
    this.intializeRoutes();
  }
  intializeRoutes() {
    this.router.route('/')
      .post(
        this.ChecksValidator.validateBody(checksSchema),
        this.ChecksCtrl.saveChecks
      );

    this.router.route('/BulkPrint')
      .post(
        this.ChecksBulkValidator.validateBody(checksBulkSchema),
        this.ChecksCtrl.saveChecksBulk
      );
    
  }
}

export default new CheksRoutes().router;