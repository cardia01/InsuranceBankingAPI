import { Router } from 'express';
import PadBCCtrl from '../controllers/PadBCCtrl';
import { PadBCValidator, padBCSchema } from '../validators/PadBCValidator';

class PadBCsRoutes {
  router = Router();
  PadBCCtrl = new PadBCCtrl();
  PadBCValidator = new PadBCValidator();

  constructor() {
    this.intializeRoutes();
  }
  intializeRoutes() {
    this.router.route('/')
      .post(
        this.PadBCValidator.validateBody(padBCSchema),
        this.PadBCCtrl.savePadBC
      );
    
  }
}

export default new PadBCsRoutes().router;