import { Sequelize, Op } from 'sequelize';
import { BulkChequeDetail } from '../models/BulkChequeDetail';

class BulkChequeDetailRepo {
  constructor() { }

  createBulkChequeDetail(props: any) {
    return BulkChequeDetail.create(props);
  }

  getBulkChequeDetailBulk(props: any) {
    return BulkChequeDetail.findAll({
      attributes: [
          [Sequelize.fn('DISTINCT', Sequelize.col('checkNumber')) ,'checkNumber'],
      ],
      where: {
        [Op.and]: [
          { vendorPublicID:  props.vendorPublicID },
          { processStatus: props.processStatus },
          { ProcessedDate: null }
        ]
      }
    });
  }
}

export default new BulkChequeDetailRepo();