import { PaymentTransaction } from '../models/PaymentTransaction';
import { Op } from "sequelize";

class DataBankingRepo {
    constructor() { }
  
    getDataBanking(options:any) {
      const fromDate = new Date(options.startDate);
      const toDate = new Date(options.endDate);

      return PaymentTransaction.findAll({
        limit: 1000 ,
        where: {
          file_date: { 
            [Op.between]: [fromDate, toDate]
          }
        },
        order: [
          ['batch_FI_Number', 'ASC'],
          ['value_Date', 'ASC']
        ]
      });

    }
  }
  
  export default new DataBankingRepo();
  