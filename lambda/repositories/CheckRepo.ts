import { CCBCChecks } from '../models/CCBCChecks';
import { CCBulkCheque } from '../models/CCBulkCheque';

class CheckRepo {
  constructor() { }

  createCheck(props: any) {
    return CCBCChecks.create(props);
  }

  createCheckBulk(props: any) {
    return CCBulkCheque.create(props);
  }
}

export default new CheckRepo();
