import { PadBC } from '../models/PadBC';

class PadBCRepo {
  constructor() { }

  createPadBC(props: any) {
    return PadBC.create(props);
  }
}

export default new PadBCRepo();
