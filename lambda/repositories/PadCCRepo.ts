import { PadCC } from '../models/PadCC';

class PadCCRepo {
  constructor() { }

  createPadCC(props: any) {
    return PadCC.create(props);
  }
}

export default new PadCCRepo();
