import serverless from "serverless-http";
import express from 'express';
import { Application } from 'express';

import Server from './index';

const app: Application = express();
const server: Server = new Server(app);

export const handler = serverless(app)