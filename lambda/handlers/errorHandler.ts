import { Request, Response, NextFunction } from 'express';

export function unCoughtErrorHandler(
  err: any,
  req: Request,
  res: Response,
  next: NextFunction,
) {
  res.end({ error: err });
}
