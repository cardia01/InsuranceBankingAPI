import { Application, urlencoded, json } from 'express';

import { unCoughtErrorHandler } from './handlers/errorHandler';
import Routes from './routes';

export default class Server {
  constructor(app: Application) {
    this.config(app);
    new Routes(app);
  }

  public config(app: Application): void {
    app.use(urlencoded({ extended: true }));
    app.use(json());
    app.use(unCoughtErrorHandler);
  }
}
