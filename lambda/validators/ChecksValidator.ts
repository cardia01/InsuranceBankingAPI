import * as Joi from 'joi';
import { Request, Response, NextFunction } from 'express';

export interface ChecksRequest extends Request {
  value?: { body?: string };
}
export class ChecksValidator {
  constructor() { }

  validateBody(schema: any) {
    return async (req: ChecksRequest, res: Response, next: NextFunction) => {
      try {
        let body = {};
        if (req.body instanceof Buffer) {
          body = JSON.parse(req.body.toString());
        } else {
          body = req.body;
        }
        const val = await schema.validateAsync(body);
        req.value = req.value ?? {};
        req.value.body = req.value.body ?? val;
        next();
      } catch (error) {
        res.status(400).json(error);
      }
    };
  }
}

export const checksSchema = Joi.object().keys({
  publicID: Joi.string().trim().required(),
  sourceSystem: Joi.string().trim().max(255).required(),
  amount: Joi.number().required(),
  checkType: Joi.string().trim().max(1).required(),
  checkDate: Joi.string().trim().max(8).required(),
  checkNumber: Joi.string().trim().max(18).required(),
  payee_Line_1: Joi.string().trim().max(40).required(),
  payee_Line_2: Joi.string().trim().max(40).allow('', null).empty(['', null]).default(''),
  payee_Line_3: Joi.string().trim().max(40).allow('', null).empty(['', null]).default(''),
  payee_Line_4: Joi.string().trim().max(40).allow('', null).empty(['', null]).default(''),
  payee_Line_5: Joi.string().trim().max(40).allow('', null).empty(['', null]).default(''),
  claimPolicyNum: Joi.string().trim().max(13).required(),
  chequeNote1: Joi.string().trim().max(34).allow('', null).empty(['', null]).default(''),
  chequeNote2: Joi.string().trim().max(34).allow('', null).empty(['', null]).default(''),
  createDate: Joi.date(),
  senderMsgID: Joi.string().trim().max(255).required(),
  processedDate: Joi.date(),
  uwCompany: Joi.string().trim().max(20),
  reason: Joi.string().trim().max(100),
  policyNo: Joi.string().trim().max(10),
  claimNo: Joi.string().trim().max(11),
  accountNo: Joi.string().trim().max(20),
  refNo: Joi.string().trim().max(20),
  category: Joi.string().trim().max(20),
  exposureType: Joi.string().trim().max(100),
  lossDate: Joi.date(),
  adjusterFirst: Joi.string().trim().max(50),
  adjusterLast: Joi.string().trim().max(50),
  adjusterBranchNo: Joi.string().trim().max(20),
  paymentType: Joi.string().trim().max(20),
  insuredName: Joi.string().trim().max(100),
  cc_bc_dateStamp: Joi.date(),
});
