import * as Joi from 'joi';
import { Request, Response, NextFunction } from 'express';

export interface PadBCRequest extends Request {
  value?: { body?: string };
}
export class PadBCValidator {
  constructor() { }

  validateBody(schema: any) {
    return async (req: PadBCRequest, res: Response, next: NextFunction) => {
      try {
        let body = {};
        if (req.body instanceof Buffer) {
          body = JSON.parse(req.body.toString());
        } else {
          body = req.body;
        }
        const val = await schema.validateAsync(body);
        req.value = req.value ?? {};
        req.value.body = req.value.body ?? val;
        next();
      } catch (error) {
        res.status(400).json(error);
      }
    };
  }
}

export const padBCSchema = Joi.object().keys({
  amount: Joi.number().required(),
  insuredName: Joi.string().trim().max(255).required(),
  termNumber: Joi.number().integer().max(2147483647).required(),
  policyEffDate: Joi.date().required(),
  policyNumber: Joi.string().trim().max(255).required(),
  publicID: Joi.string().trim().max(20).required(),
  bankNumber: Joi.string().trim().max(4).required(),
  bankBranchNumber: Joi.string().trim().max(5).required(),
  bankAccountNumber: Joi.string().trim().max(18).required(),
  bankAccountHolderName: Joi.string().trim().max(50).required(),
  requestDate: Joi.date().required(),
  dueDate: Joi.date().required(),
  processedDate: Joi.date(),
  recordType: Joi.string().trim().max(1),
  fGWProcessDate: Joi.date(),
  uwCompany: Joi.string().trim().max(20),
  bc_dateStamp: Joi.date(),
});
