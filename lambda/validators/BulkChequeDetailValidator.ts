import * as Joi from 'joi';
import { Request, Response, NextFunction } from 'express';

export interface BulkChequeDetailRequest extends Request {
  value?: { body?: string };
}
export class BulkChequeDetailValidator {
  constructor() { }

  validateBody(schema: any) {
    return async (req: BulkChequeDetailRequest, res: Response, next: NextFunction) => {
      try {
        let body = {};
        if (req.body instanceof Buffer) {
          body = JSON.parse(req.body.toString());
        } else {
          body = req.body;
        }
        const val = await schema.validateAsync(body);
        req.value = req.value ?? {};
        req.value.body = req.value.body ?? val;
        next();
      } catch (error) {
        res.status(400).json(error);
      }
    };
  }
}

export const bulkChequeDetailSchema = Joi.object().keys({
  publicID: Joi.string().trim().required(),
  checkNumber: Joi.string().trim().max(18).required(),
  claimNo: Joi.string().trim().max(11),
  refNo: Joi.string().trim().max(20),
  createDate: Joi.date(),
  amount: Joi.number().required(),
  processedDate: Joi.date(),
  payee_Line_1: Joi.string().trim().max(40).required(),
  payee_Line_2: Joi.string().trim().max(40).allow('', null).empty(['', null]).default(''),
  payee_Line_3: Joi.string().trim().max(40).allow('', null).empty(['', null]).default(''),
  payee_Line_4: Joi.string().trim().max(40).allow('', null).empty(['', null]).default(''),
  payee_Line_5: Joi.string().trim().max(40).allow('', null).empty(['', null]).default(''),
  processStatus:  Joi.string().trim().max(20).required(),
  vendorPublicID: Joi.string().trim().max(255).required(),
  dateStamp: Joi.date(),
  cc_dateStamp: Joi.date(),
});

export const getBulkChequeDetailSchema = Joi.object().keys({
  vendorPublicID: Joi.string().trim().max(255).required(),
  processStatus:  Joi.string().trim().max(20).required(),
});