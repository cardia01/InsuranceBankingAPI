import * as Joi from 'joi';
import { Request, Response, NextFunction } from 'express';

export interface DataBankingRequest extends Request {
  value?: { body?: string };
}
export class DataBankingValidator {
  constructor() { }

  validateBody(schema: any) {
    return async (req: DataBankingRequest, res: Response, next: NextFunction) => {
      try {
        let body = {};
        if (req.body instanceof Buffer) {
          body = JSON.parse(req.body.toString());
        } else {
          body = req.body;
        }
        const val = await schema.validateAsync(body);
        req.value = req.value ?? {};
        req.value.body = req.value.body ?? val;
        next();
      } catch (error) {
        res.status(400).json(error);
      }
    };
  }
}

export const dataBankingSchema = Joi.object().keys({
  startDate: Joi.date().required(),
  endDate: Joi.date().required(),
});
