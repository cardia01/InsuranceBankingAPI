import * as Joi from 'joi';
import { Request, Response, NextFunction } from 'express';

export interface PadCCRequest extends Request {
  value?: { body?: string };
}
export class PadCCValidator {
  constructor() { }

  validateBody(schema: any) {
    return async (req: PadCCRequest, res: Response, next: NextFunction) => {
      try {
        let body = {};
        if (req.body instanceof Buffer) {
          body = JSON.parse(req.body.toString());
        } else {
          body = req.body;
        }
        const val = await schema.validateAsync(body);
        req.value = req.value ?? {};
        req.value.body = req.value.body ?? val;
        next();
      } catch (error) {
        res.status(400).json(error);
      }
    };
  }
}

export const padCCSchema = Joi.object().keys({
  amount: Joi.number().required(),
  claimantName: Joi.string().trim().max(255).required(),
  claimNumber: Joi.string().trim().max(255).required(),
  paymentID: Joi.string().trim().max(20).required(),
  bankNumber: Joi.string().trim().max(4).required(),
  bankBranchNumber: Joi.string().trim().max(5).required(),
  bankAccountNumber: Joi.string().trim().max(18).required(),
  bankAccountHolderName: Joi.string().trim().max(50).required(),
  requestDate: Joi.date().required(),
  dueDate: Joi.date().required(),
  processedDate: Joi.date(),
  recordType: Joi.string().trim().max(1),
  cc_dateStamp: Joi.date(),
});
