import { Sequelize, DataTypes, Options } from 'sequelize';

// Defiened credential variable to connect into the db
const dbUrl: string = process.env.dbUrl! || 'localhost';
const dbname: string = process.env.dbname! || 'GW_Integration_local';
const user: string = process.env.user! || 'pc_user';
const pass: string = process.env.pass! || 'Guidewire.2016';
const port: string = process.env.port! || '1433';
const nodeEnv: string = process.env.node_env || 'local';

// validate the dburl to connect into db.
if (!dbUrl) {
  console.log('The dbUrl variable must not be empty or null!');
  process.exit(0);
}

// Z here means current timezone, _not_ UTC 
// return date.format('YYYY-MM-DD HH:mm:ss.SSS Z');
DataTypes.DATE.prototype._stringify = function _stringify(date: any, options: any) {
  date = this._applyTimezone(date, options);
  return date.format('YYYY-MM-DD HH:mm:ss.SSS');
};

// Configuration object to inicialize the sequelize instance
let configObj: object = {
  define: {
    createdAt: false,
    updatedAt: false,
  },
  host: dbUrl,
  port: +port,
  logging: console.log,
  dialect: "mssql",
  dialectOptions: {
    encrypt: false,
    options: {
      enableArithAbort: false,
      cryptoCredentialsDetails: {
          minVersion: 'TLSv1'
      }
    }
  }
};

// Validate if the enviroment is prod to disable the console log event
if (nodeEnv && nodeEnv === 'prod') {
  configObj = Object.assign(configObj, { logging: false });
}

// Put theconfiguration object into Options sequelize
const options: Options = configObj;

// new Sequelize module to connect with mssql
export const sequelize: Sequelize = new Sequelize(dbname!, user!, pass, options);

// sequelize connection, auth and validate db state
sequelize
  .authenticate()
  .then(() => {
    console.info('Connection has been established successfully..');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });
