import { Response, NextFunction } from 'express';
import CheckRepo from '../repositories/CheckRepo';
import { ChecksBulkRequest } from '../validators/ChecksBulkValidator';
import { ChecksRequest } from '../validators/ChecksValidator';

export default class ChecksCtrl {
  constructor() { }

  async saveChecks(req: ChecksRequest, res: Response, next: NextFunction) {
    try {
      const result = await CheckRepo.createCheck(req.value?.body);
      res.json(result);
    } catch (error) {
      console.error('Error executing the query: ', error);
      res.status(400).json(error);
    }
  }

  async saveChecksBulk(req: ChecksBulkRequest, res: Response, next: NextFunction) {
    try {
      const result = await CheckRepo.createCheckBulk(req.value?.body);
      res.json(result);
    } catch (error) {
      console.error('Error executing the query: ', error);
      res.status(400).json(error);
    }
  }
  
}
