import { Response, NextFunction } from 'express';
import DataBankingRepo from '../repositories/DataBankingRepo';
import { DataBankingRequest } from '../validators/DataBankingValidator';

export default class DataBankingCtrl {
  constructor() { }

  async getDataBanking(req: DataBankingRequest, res: Response, next: NextFunction) {
    try {
      const result = await DataBankingRepo.getDataBanking(req.value?.body);
      res.json(result);
    } catch (error) {
      console.error('Error executing the query: ', error);
      res.status(400).json(error);
    }
  }
  
}
