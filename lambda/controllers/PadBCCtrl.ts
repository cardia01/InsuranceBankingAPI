import { Response, NextFunction } from 'express';
import PadBCRepo from '../repositories/PadBCRepo';
import { PadBCRequest } from '../validators/PadBCValidator';

export default class PadBCCtrl {
  constructor() { }

  async savePadBC(req: PadBCRequest, res: Response, next: NextFunction) {
    try {
      const result = await PadBCRepo.createPadBC(req.value?.body);
      res.json(result);
    } catch (error) {
      console.error('Error executing the query: ', error);
      res.status(400).json(error);
    }
  }
  
}
