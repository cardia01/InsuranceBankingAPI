import { Response, NextFunction } from 'express';
import BulkChequeDetailRepo from '../repositories/BulkChequeDetailRepo';
import { BulkChequeDetailRequest } from '../validators/BulkChequeDetailValidator';

export default class BulkChequeDetailCtrl {
  constructor() { }

  async saveBulkChequeDetail(req: BulkChequeDetailRequest, res: Response, next: NextFunction) {
    try {
      const result = await BulkChequeDetailRepo.createBulkChequeDetail(req.value?.body);
      res.json(result);
    } catch (error) {
      console.error('Error executing the query: ', error);
      res.status(400).json(error);
    }
  }

  async getBulkChequeDetail(req: BulkChequeDetailRequest, res: Response, next: NextFunction) {
    try {
      const result = await BulkChequeDetailRepo.getBulkChequeDetailBulk(req.value?.body);
      res.json(result);
    } catch (error) {
      console.error('Error executing the query: ', error);
      res.status(400).json(error);
    }
  }
  
}
