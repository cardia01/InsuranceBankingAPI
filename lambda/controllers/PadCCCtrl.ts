import { Response, NextFunction } from 'express';
import PadCCRepo from '../repositories/PadCCRepo';
import { PadCCRequest } from '../validators/PadCCValidator';

export default class PadCCCtrl {
  constructor() { }

  async savePadCC(req: PadCCRequest, res: Response, next: NextFunction) {
    try {
      const result = await PadCCRepo.createPadCC(req.value?.body);
      res.json(result);
    } catch (error) {
      console.error('Error executing the query: ', error);
      res.status(400).json(error);
    }
  }
  
}
