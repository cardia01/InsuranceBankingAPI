import { Construct } from "constructs";
import { IPropsApp } from "../models/IPropsApp";
import { NodejsFunction } from "aws-cdk-lib/aws-lambda-nodejs";
import { Runtime } from "aws-cdk-lib/aws-lambda";
import { Duration } from "aws-cdk-lib";
import * as apigw from 'aws-cdk-lib/aws-apigateway';
import { SubnetType, Vpc } from "aws-cdk-lib/aws-ec2";

export class LambdaHelper {

  scope: Construct;
  props: IPropsApp;

  constructor(scope: Construct, props: IPropsApp) {
    this.scope = scope;
    this.props = props;

    // defines the vpc base on app properties to associate the vpc with acces to the on prem db.
    const vpc = Vpc.fromLookup(this.scope, 'VpcLookup' + props.vpc.vpcId, {vpcId: props.vpc.vpcId});

    // defines the subnets base on app properties to associate the vpc with acces to the on prem db
    const vpcSubnets = vpc.selectSubnets({
        subnetType: SubnetType.PRIVATE_ISOLATED
    }).subnets.filter(e => e.subnetId === props.vpc.subnetId1 || e.subnetId === props.vpc.subnetId2)

    // defines an AWS Lambda resource
    const app = new NodejsFunction(this.scope, props.lambda.lambdaName, {
        environment: { 
          dbUrl: props.credentialdb.dbUrl,
          dbname: props.credentialdb.dbname,
          user: props.credentialdb.user,
          pass: props.credentialdb.pass,
          port:  `${props.credentialdb.port}`,
          node_env:  `${props.propsBase?.environment}`,
        },
        bundling: {
          externalModules: [],
          minify: true,
          nodeModules: ['express', 'serverless-http', 'joi', 'sequelize', 'tedious'],
        },
        vpc,
        vpcSubnets: {
            subnets: vpcSubnets
        },
        entry: props.lambda.lambdaFolder,
        handler: props.lambda.handlerName,
        memorySize: 3000,
        runtime: Runtime.NODEJS_14_X,
        timeout: Duration.minutes(15),
      });

    // defines an API Gateway REST API resource backed by our "app" function.
    new apigw.LambdaRestApi(this.scope, props.lambda.apiGatewayName, {
      handler: app
    });
  }
}