import { StackProps } from "aws-cdk-lib";

export interface IPropsBase extends StackProps {
	propsBase?: IProps
}

export interface IProps{
	applicationName: string;
	environment: string;
}