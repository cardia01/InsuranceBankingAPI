import { IPropsBase } from "./IPropsBase";

export interface IPropsApp extends IPropsBase {
    pipeline: Ipipeline,
    lambda: Ilambda,
    vpc: IVpc,
    credentialdb: ICredentialDB,
}

interface Ipipeline {    
    pipelineName: string,
    repositoryName: string,
    branchName: string,
    primaryOutputDirectory: string,
}

interface Ilambda {    
    lambdaName: string,
    lambdaFolder: string,
    apiGatewayName: string,
    handlerName: string,
}

interface IVpc {    
    vpcId: string,
    subnetId1: string,
    subnetId2: string,
}

interface ICredentialDB {    
    dbUrl: string,
    dbname: string,
    user: string,
    pass: string,
    port: number,
}