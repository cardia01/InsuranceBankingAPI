import { Stack } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { LambdaHelper } from '../utils/lambda-helper';
import { IPropsApp } from '../models/IPropsApp';

export class InsuranceBankingApiStack extends Stack {
  constructor(scope: Construct, id: string, props: IPropsApp) {
    super(scope, id, props);

    // defines the lambda fu8ction into a helper module.
    const lambdaHelper = new LambdaHelper(this, props);

  }
}
